## Checklist

- [ ] Created new file in `po`-directory
- [ ] Updated `po/LINGUAS` with the new language
- [ ] Can continue to update the localization file if asked

## Contact

<!-- How should I reach you when I plan a new release and want to update the translations? 
Uncomment and answer any of the ways. --> 
<!-- GitLab: -->
<!-- Matrix: -->
<!-- E-Mail: -->

/label ~internationalization
